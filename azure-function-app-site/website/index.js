const fs    = require('fs');
const path  = require('path');

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    // Construct the file path
    const filePath = path.join(context.executionContext.functionDirectory, 'index.html');

    // Read the file
    var body = fs.readFileSync(filePath, 'utf-8', function(err, data) {
        return data.toString()
    });

    // Add the body to the response
    context.res = {
        status: 200,
        body: body,
        headers: {
            'Content-Type': 'text/html'
        },
        isBase64Encoded: false
    }
};